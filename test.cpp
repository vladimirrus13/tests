#include "pch.h"
#include "../test/Settings.cpp"
#include "../test/crc16.cpp"
#include "../test/DataRoutines.cpp"

TEST(SettingConstructors, DefaultTagMESSAGETYPE) {
	CSettings::MESSAGETYPE testMes;

	EXPECT_EQ(testMes.m_wType, 0);
	EXPECT_EQ(testMes.m_wMaxLength, 0);
	EXPECT_EQ(testMes.m_wDestination, 0);
	EXPECT_EQ(testMes.m_wSource, 0);
	EXPECT_EQ(testMes.m_wDestMask, 0);
	EXPECT_EQ(testMes.m_wSrcMask, 0);
}

TEST(SettingConstructors, ParamTagMESSAGETYPE) {
	CSettings::MESSAGETYPE testMes(1,2,3,4,5,6);

	EXPECT_EQ(testMes.m_wType, 1);
	EXPECT_EQ(testMes.m_wMaxLength, 2);
	EXPECT_EQ(testMes.m_wDestination, 3);
	EXPECT_EQ(testMes.m_wSource, 4);
	EXPECT_EQ(testMes.m_wDestMask, 5);
	EXPECT_EQ(testMes.m_wSrcMask, 6);
}

TEST(SettingConstructors, Default) {
  CSettings settings;
  
  EXPECT_EQ(settings.m_dwPollingPeriod, 1000);
  EXPECT_EQ(settings.m_nBufferSize, 0x90000);
  EXPECT_EQ(settings.m_nIncomingPort, 11112);
  EXPECT_EQ(settings.m_iCOMRttc, 0);
  EXPECT_EQ(settings.m_iCOMWttc, 0);
  EXPECT_EQ(settings.m_iCOMRit, -1);
  EXPECT_EQ(settings.m_wComposedType, 0x000003);
  EXPECT_EQ(settings.m_wOutputComposedType, 0x0000);
  EXPECT_EQ(settings.m_wCRC16Init, 0xFFFF);
  EXPECT_EQ(settings.m_wCPAddr, 0x0000);
  EXPECT_EQ(settings.m_wPUAddr, 0x0000);
  EXPECT_FALSE(settings.m_bTestLoopback);
  EXPECT_FALSE(settings.m_bShowSIOMessages);
  EXPECT_FALSE(settings.m_bShowMessageErrors);
  EXPECT_FALSE(settings.m_bShowCOMErrors);
  EXPECT_EQ(settings.m_strSettingsReportPath, "ugs.rep");
  EXPECT_EQ(settings.m_strCOMSetup, "COM1: baud=9600 data=8 parity=N stop=1");
  EXPECT_FALSE(settings.m_bUnpackAll);
  EXPECT_FALSE(settings.m_bMarkAll);
  EXPECT_EQ(settings.m_nStatusPeriod, 0);
  EXPECT_EQ(settings.m_iSendStatTO, 1000000);
  EXPECT_EQ(settings.m_TUType, 0x000002);
  EXPECT_EQ(settings.m_TUSrcMask, 0x0000);
  EXPECT_FALSE(settings.m_TUSrcComMsgIndex);
  EXPECT_EQ(settings.m_TUPrimToSecSrc, 1);
  EXPECT_EQ(settings.m_TUSecToPrimSrc, 1);
  EXPECT_FALSE(settings.m_bKeepLog);
  EXPECT_EQ(settings.m_wLogComposedType, 0x0000);
  EXPECT_FALSE(settings.m_bLogUnpackAll);
  EXPECT_EQ(settings.m_wLogComposedTypeToPack, 0x0000);
  EXPECT_EQ(settings.m_wSourceID, 0x000020);
  EXPECT_EQ(settings.m_wStatusRequestMessageType, 0x0001);
  EXPECT_TRUE(settings.m_arPrefix.IsEmpty());

  EXPECT_EQ(settings.m_StatusHdr.m_wType, 0x0000);
  EXPECT_EQ(settings.m_StatusHdr.m_wMaxLength, 0x20);
  EXPECT_EQ(settings.m_StatusHdr.m_wDestination, 0);
  EXPECT_EQ(settings.m_StatusHdr.m_wSource, 0);
  EXPECT_EQ(settings.m_StatusHdr.m_wDestMask, 0);
  EXPECT_EQ(settings.m_StatusHdr.m_wSrcMask, 0);

  EXPECT_EQ(settings.m_StatusMsg.m_wType, 0x000003);
  EXPECT_EQ(settings.m_StatusMsg.m_wMaxLength, 16);
  EXPECT_EQ(settings.m_StatusMsg.m_wDestination, 0);
  EXPECT_EQ(settings.m_StatusMsg.m_wSource, 0);
  EXPECT_EQ(settings.m_StatusMsg.m_wDestMask, 0);
  EXPECT_EQ(settings.m_StatusMsg.m_wSrcMask, 0);

  EXPECT_EQ(settings.m_MarkNestedMask.m_wType, 0);
  EXPECT_EQ(settings.m_MarkNestedMask.m_wMaxLength, 0);
  EXPECT_EQ(settings.m_MarkNestedMask.m_wDestination, 0);
  EXPECT_EQ(settings.m_MarkNestedMask.m_wSource, 0);
  EXPECT_EQ(settings.m_MarkNestedMask.m_wDestMask, 0);
  EXPECT_EQ(settings.m_MarkNestedMask.m_wSrcMask, 0);

  EXPECT_EQ(settings.m_MarkComposedMask.m_wType, 0);
  EXPECT_EQ(settings.m_MarkComposedMask.m_wMaxLength, 0);
  EXPECT_EQ(settings.m_MarkComposedMask.m_wDestination, 0);
  EXPECT_EQ(settings.m_MarkComposedMask.m_wSource, 0);
  EXPECT_EQ(settings.m_MarkComposedMask.m_wDestMask, 0);
  EXPECT_EQ(settings.m_MarkComposedMask.m_wSrcMask, 0);

  EXPECT_TRUE(settings.m_arStatusData.IsEmpty());

  EXPECT_EQ(settings.m_mapMsgTypes.PLookup(0x0001)->value.m_wType, 0x0001);
  EXPECT_EQ(settings.m_mapMsgTypes.PLookup(0x0001)->value.m_wMaxLength, 0x1000);
}

class TestSettings : public ::testing::Test
{
protected:
	void SetUp()
	{
		settings = new CSettings();

		settings->m_dwPollingPeriod = 1000;
		settings->m_bShowSIOMessages = false;
		settings->m_bShowMessageErrors = false;
		settings->m_bShowCOMErrors = false;
		settings->m_strSettingsReportPath, "ugs.rep";
		settings->m_nBufferSize, 0x90000;
		settings->m_bTestLoopback = false;

		settings->m_nIncomingPort = 11115;
		settings->m_iCOMRttc = 0;
		settings->m_iCOMWttc = 0;
		settings->m_iCOMRit = -1;

		settings->m_strCOMSetup = "COM1: baud=19200 data=8 parity=N stop=2";

		settings->m_wCPAddr = 0x000000;
		settings->m_wPUAddr = 0x000000;
		settings->m_arPrefix.RemoveAll();
		settings->m_arOutPrefix.RemoveAll();
		settings->m_wCRC16Init = 0xFFFF;
		settings->m_wComposedType = 0x000003;
		settings->m_wOutputComposedType = 0x0000;
		settings->m_mapMsgTypesToUnpack[0x0000] =  nullptr;
		settings->m_bUnpackAll = false;
		settings->m_mapMsgTypesToMark[0x0000] = nullptr;
		settings->m_bMarkAll = true;
		settings->m_mapMsgTypes.SetAt(0x0001, CSettings::MESSAGETYPE(0x0001, 0x1000));
		settings->m_mapMsgTypes.SetAt(0x0002, CSettings::MESSAGETYPE(0x0002, 0x1000));
		settings->m_mapMsgTypes.SetAt(0x0003, CSettings::MESSAGETYPE(0x0003, 0x1000));
		settings->m_mapMsgTypes.SetAt(0x0004, CSettings::MESSAGETYPE(0x0004, 0x1000));
		settings->m_mapMsgTypes.SetAt(0x0005, CSettings::MESSAGETYPE(0x0005, 0x1000));
		settings->m_mapMsgTypes.SetAt(0x0006, CSettings::MESSAGETYPE(0x0006, 0x1000));
		settings->m_nStatusPeriod = 1000;
		settings->m_StatusHdr.m_wType = 0x0000;
		settings->m_StatusHdr.m_wDestination = 0x0000;
		settings->m_StatusHdr.m_wSource = 0x0000;
		settings->m_StatusMsg.m_wType = 0x0003;
		settings->m_StatusMsg.m_wDestination = 0x0000;
		settings->m_StatusMsg.m_wSource = 0x0000;
		settings->m_iSendStatTO = 1000000;
		settings->m_TUType = 0x0002;
		settings->m_TUSrcMask = 0x0000;
		settings->m_TUSrcComMsgIndex = false;
		settings->m_TUPrimToSecSrc = 1;
		settings->m_TUSecToPrimSrc = 1;
		settings->m_bKeepLog = false;
		settings->m_wLogComposedType = 0x0006;
		settings->m_mapLogMsgTypesToUnpack.RemoveAll();
		settings->m_bLogUnpackAll = false;
		settings->m_wLogComposedTypeToPack = 0x0000;
		settings->m_mapLogMsgTypesToPack[0x0000] = nullptr;
		settings->m_bLogPackAll = true;

		settings->m_wSourceID = 0x0030;
		settings->m_wStatusRequestMessageType = 0x0001;
	}
	void TearDown()
	{
		delete settings;
	}

	CSettings* settings;
};

TEST_F(TestSettings, SaveTest) {
	ASSERT_NO_THROW(settings->Save("ugs2.ini"));
	EXPECT_TRUE(settings->Save("ugs2.ini"));

	EXPECT_TRUE(settings->Load("ugs2.ini"));

	EXPECT_EQ(settings->m_dwPollingPeriod, 1000);
	EXPECT_FALSE(settings->m_bShowSIOMessages);
	EXPECT_FALSE(settings->m_bShowMessageErrors);
	EXPECT_FALSE(settings->m_bShowCOMErrors);
	EXPECT_EQ(settings->m_strSettingsReportPath, "ugs.rep");
	EXPECT_EQ(settings->m_nBufferSize, 0x90000);
	EXPECT_FALSE(settings->m_bTestLoopback);

	EXPECT_EQ(settings->m_nIncomingPort, 11115);
	EXPECT_EQ(settings->m_iCOMRttc, 0);
	EXPECT_EQ(settings->m_iCOMWttc, 0);
	EXPECT_EQ(settings->m_iCOMRit, -1);

	EXPECT_EQ(settings->m_strCOMSetup, "COM1: baud=19200 data=8 parity=N stop=2");

	EXPECT_EQ(settings->m_wCPAddr, 0x000000);
	EXPECT_EQ(settings->m_wPUAddr, 0x000000);
	EXPECT_TRUE(settings->m_arPrefix.IsEmpty());
	EXPECT_TRUE(settings->m_arOutPrefix.IsEmpty());
	EXPECT_EQ(settings->m_wCRC16Init, 0xFFFF);
	EXPECT_EQ(settings->m_wComposedType, 0x000003);
	EXPECT_EQ(settings->m_wOutputComposedType, 0x0000);
	EXPECT_EQ(settings->m_mapMsgTypesToUnpack[0x0000], nullptr);
	EXPECT_FALSE(settings->m_bUnpackAll);
	EXPECT_EQ(settings->m_mapMsgTypesToMark[0x0000], nullptr);
	EXPECT_TRUE(settings->m_bMarkAll);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0001].m_wType, 0x0001);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0001].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0002].m_wType, 0x0002);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0002].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0003].m_wType, 0x0003);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0003].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0004].m_wType, 0x0004);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0004].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0005].m_wType, 0x0005);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0005].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0006].m_wType, 0x0006);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0006].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_nStatusPeriod, 1000);
	EXPECT_EQ(settings->m_StatusHdr.m_wType, 0x0000);
	EXPECT_EQ(settings->m_StatusHdr.m_wDestination, 0x0000);
	EXPECT_EQ(settings->m_StatusHdr.m_wSource, 0x0000);
	EXPECT_EQ(settings->m_StatusMsg.m_wType, 0x0003);
	EXPECT_EQ(settings->m_StatusMsg.m_wDestination, 0x0000);
	EXPECT_EQ(settings->m_StatusMsg.m_wSource, 0x0000);
	EXPECT_EQ(settings->m_iSendStatTO, 1000000);
	EXPECT_EQ(settings->m_TUType, 0x0002);
	EXPECT_EQ(settings->m_TUSrcMask, 0x0000);
	EXPECT_FALSE(settings->m_TUSrcComMsgIndex);
	EXPECT_EQ(settings->m_TUPrimToSecSrc, 1);
	EXPECT_EQ(settings->m_TUSecToPrimSrc, 1);
	EXPECT_FALSE(settings->m_bKeepLog);
	EXPECT_EQ(settings->m_wLogComposedType, 0x0006);
	EXPECT_TRUE(settings->m_mapLogMsgTypesToUnpack.IsEmpty());
	EXPECT_FALSE(settings->m_bLogUnpackAll);
	EXPECT_EQ(settings->m_wLogComposedTypeToPack, 0x0000);
	EXPECT_EQ(settings->m_mapLogMsgTypesToPack[0x0000], nullptr);
	EXPECT_TRUE(settings->m_bLogPackAll);

	EXPECT_EQ(settings->m_wSourceID, 0x0030);
	EXPECT_EQ(settings->m_wStatusRequestMessageType, 0x0001);
}


TEST_F(TestSettings, LoadTest) {
	ASSERT_NO_THROW(settings->Load("ugs.in"));
	EXPECT_FALSE(settings->Load("ugs.in"));
	EXPECT_TRUE(settings->Load("ugs.ini"));

	EXPECT_EQ(settings->m_dwPollingPeriod, 1000);
	EXPECT_FALSE(settings->m_bShowSIOMessages);
	EXPECT_FALSE(settings->m_bShowMessageErrors);
	EXPECT_FALSE(settings->m_bShowCOMErrors);
	EXPECT_EQ(settings->m_strSettingsReportPath, "ugs.rep");
	EXPECT_EQ(settings->m_nBufferSize, 0x90000);
	EXPECT_FALSE(settings->m_bTestLoopback);

	EXPECT_EQ(settings->m_nIncomingPort, 11115);
	EXPECT_EQ(settings->m_iCOMRttc, 0);
	EXPECT_EQ(settings->m_iCOMWttc, 0);
	EXPECT_EQ(settings->m_iCOMRit, -1);

	EXPECT_EQ(settings->m_strCOMSetup, "COM1: baud=19200 data=8 parity=N stop=2");

	EXPECT_EQ(settings->m_wCPAddr, 0x000000);
	EXPECT_EQ(settings->m_wPUAddr, 0x000000);
	EXPECT_TRUE(settings->m_arPrefix.IsEmpty());
	EXPECT_TRUE(settings->m_arOutPrefix.IsEmpty());
	EXPECT_EQ(settings->m_wCRC16Init, 0xFFFF);
	EXPECT_EQ(settings->m_wComposedType, 0x000003);
	EXPECT_EQ(settings->m_wOutputComposedType, 0x0000);
	EXPECT_EQ(settings->m_mapMsgTypesToUnpack[0x0000], nullptr);
	EXPECT_FALSE(settings->m_bUnpackAll);
	EXPECT_EQ(settings->m_mapMsgTypesToMark[0x0000], nullptr);
	EXPECT_TRUE(settings->m_bMarkAll);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0001].m_wType, 0x0001);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0001].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0002].m_wType, 0x0002);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0002].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0003].m_wType, 0x0003);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0003].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0004].m_wType, 0x0004);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0004].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0005].m_wType, 0x0005);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0005].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0006].m_wType, 0x0006);
	EXPECT_EQ(settings->m_mapMsgTypes[0x0006].m_wMaxLength, 0x1000);
	EXPECT_EQ(settings->m_nStatusPeriod, 1000);
	EXPECT_EQ(settings->m_StatusHdr.m_wType, 0x0000);
	EXPECT_EQ(settings->m_StatusHdr.m_wDestination, 0x0000);
	EXPECT_EQ(settings->m_StatusHdr.m_wSource, 0x0000);
	EXPECT_EQ(settings->m_StatusMsg.m_wType, 0x0003);
	EXPECT_EQ(settings->m_StatusMsg.m_wDestination, 0x0000);
	EXPECT_EQ(settings->m_StatusMsg.m_wSource, 0x0000);
	EXPECT_EQ(settings->m_iSendStatTO, 1000000);
	EXPECT_EQ(settings->m_TUType, 0x0002);
	EXPECT_EQ(settings->m_TUSrcMask, 0x0000);
	EXPECT_FALSE(settings->m_TUSrcComMsgIndex);
	EXPECT_EQ(settings->m_TUPrimToSecSrc, 1);
	EXPECT_EQ(settings->m_TUSecToPrimSrc, 1);
	EXPECT_FALSE(settings->m_bKeepLog);
	EXPECT_EQ(settings->m_wLogComposedType, 0x0006);
	EXPECT_TRUE(settings->m_mapLogMsgTypesToUnpack.IsEmpty());
	EXPECT_FALSE(settings->m_bLogUnpackAll);
	EXPECT_EQ(settings->m_wLogComposedTypeToPack, 0x0000);
	EXPECT_EQ(settings->m_mapLogMsgTypesToPack[0x0000], nullptr);
	EXPECT_TRUE(settings->m_bLogPackAll);

	EXPECT_EQ(settings->m_wSourceID, 0x0030);
	EXPECT_EQ(settings->m_wStatusRequestMessageType, 0x0001);
}
